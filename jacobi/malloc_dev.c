#include <omp.h>
#include <stdlib.h>
#include <stdio.h>

#define N 100

int main() {
    #pragma omp target
    {
    char *dptr = malloc(N);
    printf("%d\n", *dptr);
    free(dptr);
    }
}
