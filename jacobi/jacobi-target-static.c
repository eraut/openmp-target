#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define N 100

int main(int argc, char *argv[])
{

    double A[N][N], Anew[N][N];

    // Fill with random data
    srand(1);
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            A[i][j] = ((double)rand())/RAND_MAX;
        }
    }

    const double tol = 0.01;
    double error = 1000.0;

    // Timekeeping
    struct timespec start,end;
    clock_gettime(CLOCK_MONOTONIC, &start);

    #pragma omp target data map(to:A) map(tofrom:error) map(alloc:Anew)
    {
        const int iter_max = 10000;
        int iter = 0;

        while ( error > tol && iter < iter_max ) {
            error = 0.0;
            #pragma omp target map(always,tofrom:error)
            #pragma omp teams distribute parallel for reduction(max:error) collapse(2)
            for (int i = 1; i < N-1; i++) {
                for (int j = 1; j < N-1; j++) {
                    Anew[i][j] = 0.25 * ( A[i][j+1] + A[i][j-1] +
                                          A[i-1][j] + A[i+1][j] );
                    error = fmax(error, fabs(Anew[i][j] - A[i][j]));
                }
            }
            ++iter;
            if (iter % 100 == 0)
                printf("Iteration %d, error = %g\n", iter, error);

            #pragma omp target teams distribute parallel for collapse(2)
            for (int i = 1; i < N-1; i++) {
                for (int j = 1; j < N-1; j++) {
                    A[i][j] = Anew[i][j];
                }
            }
        }
    }

    clock_gettime(CLOCK_MONOTONIC, &end);
    printf("Final error: %g\n", error);
    double duration = (end.tv_sec  - start.tv_sec) +
                    (double)(end.tv_nsec - start.tv_nsec) / 1.0e9;
    printf("Time elapsed: %g sec\n", duration);
}
