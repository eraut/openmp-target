#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char *argv[])
{
    double x = 1000.0;
    #pragma omp parallel reduction(+:x)
    x = 1.0;

    printf("%g\n", x);
}
