#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

int main(int argc, char *argv[])
{
    int n;
    if (argc >= 2)
        n = atoi(argv[1]);
    else
        n = 100;
    double *A = malloc(sizeof(double)*n*n);
    double *Anew = malloc(sizeof(double)*n*n);

    // Fill with random data
    srand(1);
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            A[i*n+j] = ((double)rand())/RAND_MAX;
        }
    }

    const double tol = 0.01;
    const int iter_max = 10000;

    // Timekeeping
    struct timespec start,end;
    clock_gettime(CLOCK_MONOTONIC, &start);

    double error = 1000.0;
    int iter = 0;
    while ( error > tol && iter < iter_max ) {

        error = 0.0;
#pragma omp parallel for reduction(max:error) collapse(2)
        for (int i = 1; i < n-1; i++) {
            for (int j = 1; j < n-1; j++) {
                Anew[i*n+j] = 0.25 * ( A[i*n+j+1] + A[i*n+j-1] +
                                      A[(i-1)*n+j] + A[(i+1)*n+j] );
                error = fmax(error, fabs(Anew[i*n+j] - A[i*n+j]));
            }
        }
        ++iter;
        if (iter % 100 == 0)
            printf("Iteration %d, error = %g\n", iter, error);

#pragma omp parallel for collapse(2)
        for (int i = 1; i < n-1; i++) {
            for (int j = 1; j < n-1; j++) {
                A[i*n+j] = Anew[i*n+j];
            }
        }
    }

    clock_gettime(CLOCK_MONOTONIC, &end);
    printf("Final error: %g\n", error);
    double duration = (end.tv_sec  - start.tv_sec) +
                    (double)(end.tv_nsec - start.tv_nsec) / 1.0e9;
    printf("Time elapsed: %g sec\n", duration);

    free(A);
    free(Anew);
}
