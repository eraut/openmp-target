#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char *argv[])
{
    int dev = omp_get_default_device();
    int n = 10;

    char *dptr = omp_target_alloc(n,dev);
    if (!dptr) {
        printf("Couldn't allocate on device %d\n", dev);
    }
    printf("%p\n", dptr);
    #pragma omp target is_device_ptr(dptr) device(dev)
    for (int i = 0; i < n; i++)
        dptr[i] = i;

    printf("%p\n", dptr);
}
