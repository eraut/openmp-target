#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char *argv[])
{
    double error = 1000.0;
    #pragma omp target data map(tofrom:error)
    {
        #pragma omp target teams map(error)
        {
            error = 0.0;
            #pragma omp distribute parallel for reduction(max:error)
            for (int i = 0; i < 40000; i++) {
                error = fmax(error, 42.0);
            }
        }
    }
    printf("%g\n", error);
}
