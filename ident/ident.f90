program main

  use omp_lib

  implicit none

  logical :: is_initial_device
  integer :: num_teams_dev, num_threads_dev

  !$omp parallel
  !$omp single
  print *, "Num threads on host: ", omp_get_num_threads()
  !$omp end single
  !$omp end parallel

  print *, "Num devices: ", omp_get_num_devices()
  print *, "Initial device: ", omp_get_initial_device()
  print *, "Default device: ", omp_get_default_device()

  !$omp target teams map(from: is_initial_device, num_teams_dev, &
  !$omp num_threads_dev)
  if (omp_get_team_num() == 0) then
    is_initial_device = omp_is_initial_device()
    num_teams_dev = omp_get_num_teams()
    !$omp parallel
    !$omp single
    num_threads_dev = omp_get_num_threads()
    !$omp end single
    !$omp end parallel
  end if
  !$omp end target teams

  print *, "Is initial device (in target region): ", is_initial_device
  print *, "Num teams in device: ", num_teams_dev
  print *, "Num threads in device: ", num_threads_dev

end program main
