#include <omp.h>
#include <stdio.h>

int main()
{
  #pragma omp parallel
  #pragma omp single
  printf("Num threads on host: %d\n", omp_get_num_threads());

  printf("Num devices: %d\n", omp_get_num_devices());
  printf("Initial device: %d\n", omp_get_initial_device());
  printf("Default device: %d\n", omp_get_default_device());

  #pragma omp target teams
  {
    if (omp_get_team_num() == 0) {
      printf("Is initial device (in target region): %d\n",
             omp_is_initial_device());
      printf("Num teams in device: %d\n", omp_get_num_teams());
      #pragma omp parallel
      #pragma omp single
      printf("Num threads in device: %d\n", omp_get_num_threads());
    }
  }
}
