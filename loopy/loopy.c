#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

typedef double tfloat;

int main(int argc, char *argv[])
{
    int n, T;

    if (argc >= 2) n = atoi(argv[1]);
    else n = 100L;

    if (argc >= 3) T = atoi(argv[2]);
    else T = 100L;

    tfloat *x = malloc(n*sizeof(tfloat));

    // Fill with random data
    srand(1);
    for (long i = 0; i < n; i++) {
        x[i] = ((tfloat)rand())/(tfloat)RAND_MAX;
    }

    // Timekeeping
    struct timespec start,end;
    clock_gettime(CLOCK_MONOTONIC, &start);

    #ifdef OMP_TARGET
    #pragma omp target data map(x[0:n])
    #endif
    {
        // Main loop
        for (int t = 0; t < T; t++) {
            //tfloat alpha = ((tfloat)rand())/(tfloat)RAND_MAX;
            tfloat alpha = 2.0*(tfloat)t/(tfloat)T;
            #ifdef OMP_TARGET
            #pragma omp target teams distribute parallel for
            #else
            #pragma omp parallel for
            #endif
            for (int i = 0; i < n; i++) {
                x[i] += (alpha*x[i]);
            }
        }
    }

    clock_gettime(CLOCK_MONOTONIC, &end);

    double duration = (end.tv_sec  - start.tv_sec) +
                    (double)(end.tv_nsec - start.tv_nsec) / 1.0e9;
    printf("Time elapsed: %g sec\n", duration);

    free(x);
}
