#!/bin/bash

n=1500

exes="jacobi-target_acc jacobi-target_omp_clg jacobi-target_omp_gcc"

for exe in $exes; do
    for i in `seq 1`; do
        echo "Running $exe iter $i"
        ./$exe $n
    done
done
