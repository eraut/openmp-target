#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

int main(int argc, char *argv[])
{
    int n;
    if (argc >= 2)
        n = atoi(argv[1]);
    else
        n = 100;
    const int N = n*n;

    double *A    = malloc(sizeof(double)*N),
           *Anew = malloc(sizeof(double)*N);

    // Fill A with random data
    srand(1);
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            A[i*n+j] = ((double)rand())/RAND_MAX;
        }
    }

    const double tol = 0.01;
    double error = 1000.0;

    // Timekeeping
    struct timespec start,end;
    clock_gettime(CLOCK_MONOTONIC, &start);

    #pragma omp target data map(to:A[0:N]) map(alloc:Anew[0:N],error)
    #pragma acc data copyin(A[N]) create(Anew[N])
    {
        const int iter_max = 10000;
        int iter = 0;
        while ( error > tol && iter < iter_max ) {
            error = 0.0;
            #pragma omp target map(always,tofrom:error)
            #pragma omp teams distribute parallel for simd reduction(max:error) collapse(2)
            #pragma acc parallel loop collapse(2) reduction(max:error)
            for (int i = 1; i < n-1; i++) {
                for (int j = 1; j < n-1; j++) {
                    Anew[i*n+j] = 0.25 * ( A[i*n+j+1] + A[i*n+j-1] +
                                          A[(i-1)*n+j] + A[(i+1)*n+j] );
                    error = fmax(error, fabs(Anew[i*n+j] - A[i*n+j]));
                }
            }

            // Update A
            #pragma omp target teams distribute parallel for simd collapse(2)
            #pragma acc parallel loop collapse(2)
            for (int i = 1; i < n-1; i++) {
                for (int j = 1; j < n-1; j++) {
                    A[i*n+j] = Anew[i*n+j];
                }
            }
            ++iter;
            if (iter % 500 == 0)
                printf("Iteration %d, error = %g\n", iter, error);
        }
    }

    clock_gettime(CLOCK_MONOTONIC, &end);
    printf("Final error: %g\n", error);
    double duration = (end.tv_sec  - start.tv_sec) +
                    (double)(end.tv_nsec - start.tv_nsec) / 1.0e9;
    printf("Time elapsed: %g sec\n", duration);

    free(A);
    free(Anew);
}
