#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define IDX(i,j,k) ((i)*n2+(j)*n+(k))

int main(int argc, char *argv[])
{
    int n;
    if (argc >= 2)
        n = atoi(argv[1]);
    else
        n = 100;

    printf("n = %d\n", n);

    const int n2 = n*n;
    const int n3 = n2*n;

    double *A1 = malloc(n3*sizeof(double)),
           *A2 = malloc(n3*sizeof(double));

    double *A = A1;
    double *Anew = A2;

    // Fill A with data
    int n_places = omp_get_num_places();
    if (n_places != 2) {
        printf("NUMA_DBG omp_places not 2!\n");
        exit(1);
    }
    int max_threads = omp_get_max_threads();
    int nx1 = n/2;
    omp_set_max_active_levels(2);
    #pragma omp parallel num_threads(2) proc_bind(spread) default(shared)
    {
        int x1, x2;
        if (omp_get_place_num() == 0) {
            x1 = 0;
            x2 = nx1;
        } else { // place_num == 1
            x1 = nx1;
            x2 = n;
        }
        #pragma omp parallel for num_threads(max_threads/2) collapse(3) default(shared)
        for (int i = x1; i < x2; i++) {
            for (int j = 0; j < n; j++) {
                for (int k = 0; k < n; k++) {
                    A[IDX(i,j,k)] = 0.0;
                    Anew[IDX(i,j,k)] = 0.0;
                }
            }
        }
    }

    srand(1);
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            for (int k = 0; k < n; k++) {
                A[IDX(i,j,k)] = ((double)rand())/RAND_MAX;
                Anew[IDX(i,j,k)] = 0.0;
            }
        }
    }

    printf("Completed initialization\n");

    // Timekeeping
    struct timespec start,end;
    clock_gettime(CLOCK_MONOTONIC, &start);

    const int iter_max = 1000;
    int iter = 0;
    double error;
    while ( iter < iter_max ) {
        error = 0.0;
        #pragma omp parallel num_threads(2) proc_bind(spread) default(shared) reduction(max:error)
        {
            int x1, x2;
            if (omp_get_place_num() == 0) {
                x1 = 4;
                x2 = nx1;
            } else { // place_num == 1
                x1 = nx1;
                x2 = n-4;
            }
            error = 0.0;
            #pragma omp parallel for num_threads(max_threads/2) collapse(3) default(shared) reduction(max:error)
            for (int i = x1; i < x2; i++) {
                for (int j = 4; j < n-4; j++) {
                    for (int k = 4; k < n-4; k++) {
                        Anew[IDX(i,j,k)] = (1./24.) *
                        ( (A[IDX(i+1,j,k)] + A[IDX(i+2,j,k)] + A[IDX(i+3,j,k)] + A[IDX(i+4,j,k)]) +
                          (A[IDX(i-1,j,k)] + A[IDX(i-2,j,k)] + A[IDX(i-3,j,k)] + A[IDX(i-4,j,k)]) +
                          (A[IDX(i,j+1,k)] + A[IDX(i,j+2,k)] + A[IDX(i,j+3,k)] + A[IDX(i,j+4,k)]) +
                          (A[IDX(i,j-1,k)] + A[IDX(i,j-2,k)] + A[IDX(i,j-3,k)] + A[IDX(i,j-4,k)]) +
                          (A[IDX(i,j,k+1)] + A[IDX(i,j,k+2)] + A[IDX(i,j,k+3)] + A[IDX(i,j,k+4)]) +
                          (A[IDX(i,j,k-1)] + A[IDX(i,j,k-2)] + A[IDX(i,j,k-3)] + A[IDX(i,j,k-4)]) );
                        //printf("%.15e %.15e\n", Anew[IDX(i,j,k)], A[IDX(i,j,k)]);
                        error = fmax(error, fabs(Anew[IDX(i,j,k)]-A[IDX(i,j,k)]));
                    }
                }
            }
        }

        ++iter;
        if (iter % 100 == 0)
            printf("Iteration %d, error = %g\n", iter, error);

        // Update A
        double *t = Anew;
        Anew = A;
        A = t;
    }

    clock_gettime(CLOCK_MONOTONIC, &end);
    double duration = (end.tv_sec  - start.tv_sec) +
                    (double)(end.tv_nsec - start.tv_nsec) / 1.0e9;
    printf("Time elapsed: %g sec\n", duration);

    // Print some values so (hopefully) nothing is optimized out
    printf("%g %g %g\n", A[IDX(nx1,nx1,nx1)], A[IDX(nx1+1,nx1+1,nx1+1)], A[IDX(nx1-1,nx1-1,nx1-1)]);

    free(A1);
    free(A2);
}
