#!/bin/bash

#SBATCH -J compile-omp-offload
#SBATCH --ntasks-per-node=28
#SBATCH --nodes=1
#SBATCH --time=05:00
#SBATCH -p gpu

module load shared
module load clang/8.0.0

set -o xtrace

nvidia-smi
clang -v -fopenmp -fopenmp-targets=nvptx64-nvidia-cuda -Xopenmp-target -march=sm_35 ident.c && \
./a.out
