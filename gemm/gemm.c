#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef float tfloat;

// Testing ONLY!
void print_mtx(int n, tfloat *a)
{
    printf("[\n");
    for (int i = 0; i < n; i++) {
        printf("[");
        for (int j = 0; j < n; j++) {
            printf("%g, ", a[n*i+j]);
        }
        printf("],\n");
    }
    printf("]\n");
}

int main(int argc, char *argv[])
{
    int n;
    if (argc >= 2) n = atoi(argv[1]);
    else n = 100;

    const int N = n*n;

    tfloat *a = malloc(sizeof(tfloat)*N),
           *b = malloc(sizeof(tfloat)*N),
           *c = malloc(sizeof(tfloat)*N);

    // Fill a and b with random data
    srand(1);
    for (int i = 0; i < N; i++) {
        a[i] = ((tfloat)rand())/(tfloat)RAND_MAX;
        b[i] = ((tfloat)rand())/(tfloat)RAND_MAX;
        c[i] = ((tfloat)rand())/(tfloat)RAND_MAX;
    }

    tfloat alpha = ((tfloat)rand())/(tfloat)RAND_MAX;

    // Print for testing
    /*
    printf("a = "); print_mtx(n,a);
    printf("b = "); print_mtx(n,b);
    printf("c = "); print_mtx(n,c);
    printf("alpha = %g\n", alpha);
    */

    // Timing
    struct timespec start,end;

    #pragma omp target data map(to:a[0:N],b[0:N]) map(tofrom:c[0:N])
    {

        clock_gettime(CLOCK_MONOTONIC, &start);
        #pragma omp target teams distribute map(a[0:N],b[0:N],c[0:N]) \
            num_teams(n/10) thread_limit(n)
        // C-row loop
        for (int i = 0; i < n; i++) {
            // A-col loop
            #pragma omp parallel for
            for (int k = 0; k < n; k++) {
                tfloat temp = alpha * a[n*i+k];

                // C-col loop
                for (int j = 0; j < n; j++) {
                    c[n*i+j] += temp*b[n*k+j];
                }
            }
        }

        clock_gettime(CLOCK_MONOTONIC, &end);
    }
    //printf("c (new) = "); print_mtx(n,c);

    double duration = (end.tv_sec  - start.tv_sec) +
                    (double)(end.tv_nsec - start.tv_nsec) / 1.0e9;
    printf("Time elapsed: %g sec\n", duration);

    // Cleanup
    free(a); free(b); free(c);
}
