program daxpy

  use omp_lib

  implicit none

  real, parameter :: alpha = 0.7
  integer :: i, n, stat
  character(len=12) :: narg

  real, allocatable :: x(:), y(:)

  double precision :: ts, te, tgpu, tcpu

  n = -10

  call get_command_argument(1, narg, status=stat)

  if ( stat == 0 ) then
    read(narg,*,iostat=stat) n
  end if

  if ( stat /= 0 ) then
    n = 100
  end if

  print *, 'n=',n

  allocate (x(n), y(n))

  print *, 'Initializing'

  ts = omp_get_wtime()
  !$omp parallel do
  do i=1,n
    x(i) = real(i)/real(n)
    y(i) = 2.0*real(i)/real(n)
  end do
  te = omp_get_wtime()
  print *, 'Initialize time', te-ts

  !$omp target data map(to:x,y)

  print *, 'Running GPU'
  ts = omp_get_wtime()
  !$omp target teams distribute parallel do
  do i=1,n
    y(i) = alpha*x(i) + y(i)
  end do
  te = omp_get_wtime()
  tgpu = te-ts
  print *, 'Time GPU:', tgpu

  print *, 'Running GPU'
  ts = omp_get_wtime()
  !$omp target teams distribute parallel do
  do i=1,n
    y(i) = alpha*x(i) + y(i)
  end do
  te = omp_get_wtime()
  tgpu = te-ts
  print *, 'Time GPU:', tgpu

  !$omp end target data

  print *, 'Running CPU'
  ts = omp_get_wtime()
  !$omp parallel do
  do i=1,n
    y(i) = alpha*x(i) + y(i)
  end do
  te = omp_get_wtime()
  tcpu = te-ts
  print *, 'Time CPU:', tcpu

  deallocate(x,y)

end program daxpy
