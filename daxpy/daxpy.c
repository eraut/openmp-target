#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

typedef float tfloat;

int main(int argc, char *argv[])
{
    long n;

    if (argc >= 2) n = atol(argv[1]);
    else n = 100L;

    printf("n = %ld\n", n);

    const tfloat alpha = 0.7;

    tfloat *restrict x = calloc(n,sizeof(tfloat)),
           *restrict y = calloc(n,sizeof(tfloat));

    // Fill with random data
    //srand(1);
    //for (long i = 0; i < n; i++) {
    //    x[i] = ((tfloat)rand())/RAND_MAX;
    //    y[i] = ((tfloat)rand())/RAND_MAX;
//        x[i] = (tfloat)i;
//        y[i] = (tfloat)i/(tfloat)2.0;
    //}

    // Timekeeping
    struct timespec start,end;

    #pragma omp target data map(to:x[0:n], y[0:n])
    {
        clock_gettime(CLOCK_MONOTONIC, &start);
        // Main loop
        //#pragma omp parallel for
        #pragma omp target teams distribute parallel for
        for (long i = 0; i < n; i++) {
            y[i] += (alpha*x[i] * y[i]);
        }
        clock_gettime(CLOCK_MONOTONIC, &end);
    }
    double duration = (end.tv_sec  - start.tv_sec) +
                    (double)(end.tv_nsec - start.tv_nsec) / 1.0e9;
    printf("Time elapsed: %g sec\n", duration);

    free(x);
    free(y);
}
