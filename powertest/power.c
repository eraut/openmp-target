#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

int main(int argc, char *argv[])
{
    int n;
    if (argc >= 2) n = atoi(argv[1]);
    else n = 100;

    float *a = malloc(sizeof(float)*n),
          *b = malloc(sizeof(float)*n),
          *c = malloc(sizeof(float)*n);

    // Fill a and b with random data
    srand(1);
    for (int i = 0; i < n; i++) {
        a[i] = ((float)rand())/(float)RAND_MAX;
        b[i] = ((float)rand())/(float)RAND_MAX;
    }

    // Timing
    struct timespec start,end;
    clock_gettime(CLOCK_MONOTONIC, &start);

    #pragma omp target data map(to: a[0:n],b[0:n]) map(from:c[0:n])
    {

        #pragma omp target teams distribute parallel for
        for (int i = 0; i < n; i++) {
            c[i] = pow(a[i], b[i]);
        }

    }
    clock_gettime(CLOCK_MONOTONIC, &end);

    double duration = (end.tv_sec  - start.tv_sec) +
                    (double)(end.tv_nsec - start.tv_nsec) / 1.0e9;
    printf("Time elapsed: %g sec\n", duration);

    // Cleanup
    free(a); free(b); free(c);
}
