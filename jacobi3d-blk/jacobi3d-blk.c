#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define IDX(i,j,k) ((i)*n2+(j)*n+(k))

int main(int argc, char *argv[])
{
    size_t n, b;
    if (argc >= 3) {
        sscanf(argv[1], "%zu", &n);
        sscanf(argv[2], "%zu", &b);
    }
    else {
        n = 130;
        b = 32;
    }

    printf("n = %zu, b = %zu\n", n, b);

    const size_t n2 = n*n;
    const size_t n3 = n2*n;

#ifdef BLOCK
    size_t nb = (n-2)/b;
#endif

    double *A    = malloc(n3*sizeof(double)),
           *Anew = malloc(n3*sizeof(double));

    // Fill A (inners) with data
#ifdef BLOCK
    #pragma omp parallel for collapse(3)
    for (size_t ii = 0; ii < nb; ii++) {
        for (size_t jj = 0; jj < nb; jj++) {
            for (size_t kk = 0; kk < nb; kk++) {
                const size_t imin = 1+ii*b;
                const size_t imax = 1+(ii+1)*b;
                const size_t jmin = 1+jj*b;
                const size_t jmax = 1+(jj+1)*b;
                const size_t kmin = 1+kk*b;
                const size_t kmax = 1+(kk+1)*b;
                for (size_t i = imin; i < imax; i++) {
                    for (size_t j = jmin; j < jmax; j++) {
                        for (size_t k = kmin; k < kmax; k++) {
                            A[IDX(i,j,k)] = 2.0*IDX(i,j,k)/n3;
                            Anew[IDX(i,j,k)] = 0.0;
                        }
                    }
                }
            }
        }
    }
#else
    #pragma omp parallel for collapse(3)
    for (size_t i = 1; i < n-1; i++) {
        for (size_t j = 1; j < n-1; j++) {
            for (size_t k = 1; k < n-1; k++) {
                A[IDX(i,j,k)] = 2.0*IDX(i,j,k)/n3;
                Anew[IDX(i,j,k)] = 0.0;
            }
        }
    }
#endif

    printf("Initializing boundary\n");

    // Fill A (boundary) with data
    #pragma omp parallel for collapse(3)
    for (size_t i = 0; i < n; i++) {
        for (size_t j = 0; j < n; j++) {
            for (size_t k = 0; k < n; k++) {
                if (i == 0 || i == n-1 || j == 0 || j == n-1 || k == 0 ||
                    k == n-1) {
                    A[IDX(i,j,k)] = 0.0;
                    Anew[IDX(i,j,k)] = 0.0;
                }
            }
        }
    }

    const double tol = -1.0;
    double error = 1000.0;

    printf("Completed initialization\n");

    // Timekeeping
    struct timespec start,end;
    clock_gettime(CLOCK_MONOTONIC, &start);

    const int iter_max = 5;
    int iter = 0;
    while ( error > tol && iter < iter_max ) {
        error = 0.0;
#ifdef BLOCK
        #pragma omp parallel for collapse(3) reduction(max:error)
        for (size_t ii = 0; ii < nb; ii++) {
            for (size_t jj = 0; jj < nb; jj++) {
                for (size_t kk = 0; kk < nb; kk++) {
                    const size_t imin = 1+ii*b;
                    const size_t imax = 1+(ii+1)*b;
                    const size_t jmin = 1+jj*b;
                    const size_t jmax = 1+(jj+1)*b;
                    const size_t kmin = 1+kk*b;
                    const size_t kmax = 1+(kk+1)*b;
                    for (size_t i = imin; i < imax; i++) {
                        for (size_t j = jmin; j < jmax; j++) {
                            for (size_t k = kmin; k < kmax; k++) {
                                Anew[IDX(i,j,k)] = (1./6.) *
                                    (A[IDX(i+1,j,k)] + A[IDX(i-1,j,k)] +
                                     A[IDX(i,j+1,k)] + A[IDX(i,j-1,k)] +
                                     A[IDX(i,j,k+1)] + A[IDX(i,j,k-1)]);
                                error = fmax(error, fabs(Anew[IDX(i,j,k)] - A[IDX(i,j,k)]));
                            }
                        }
                    }
                }
            }
        }

        // Update A
        #pragma omp parallel for collapse(3)
        for (size_t ii = 0; ii < nb; ii++) {
            for (size_t jj = 0; jj < nb; jj++) {
                for (size_t kk = 0; kk < nb; kk++) {
                    const size_t imin = 1+ii*b;
                    const size_t imax = 1+(ii+1)*b;
                    const size_t jmin = 1+jj*b;
                    const size_t jmax = 1+(jj+1)*b;
                    const size_t kmin = 1+kk*b;
                    const size_t kmax = 1+(kk+1)*b;
                    for (size_t i = imin; i < imax; i++) {
                        for (size_t j = jmin; j < jmax; j++) {
                            for (size_t k = kmin; k < kmax; k++) {
                                A[IDX(i,j,k)] = Anew[IDX(i,j,k)];
                            }
                        }
                    }
                }
            }
        }
#else
        #pragma omp parallel for collapse(3) reduction(max:error)
        for (size_t i = 1; i < n-1; i++) {
            for (size_t j = 1; j < n-1; j++) {
                for (size_t k = 1; k < n-1; k++) {
                    Anew[IDX(i,j,k)] = (1./6.) *
                        (A[IDX(i+1,j,k)] + A[IDX(i-1,j,k)] +
                         A[IDX(i,j+1,k)] + A[IDX(i,j-1,k)] +
                         A[IDX(i,j,k+1)] + A[IDX(i,j,k-1)]);
                    error = fmax(error, fabs(Anew[IDX(i,j,k)] - A[IDX(i,j,k)]));
                }
            }
        }

        // Update A
        #pragma omp parallel for collapse(3)
        for (size_t i = 1; i < n-1; i++) {
            for (size_t j = 1; j < n-1; j++) {
                for (size_t k = 1; k < n-1; k++) {
                    A[IDX(i,j,k)] = Anew[IDX(i,j,k)];
                }
            }
        }
#endif

        ++iter;
        //if (iter % 100 == 0)
            printf("Iteration %d, error = %g\n", iter, error);
    }

    clock_gettime(CLOCK_MONOTONIC, &end);
    printf("Final error: %g\n", error);
    double duration = (end.tv_sec  - start.tv_sec) +
                    (double)(end.tv_nsec - start.tv_nsec) / 1.0e9;
    printf("Time elapsed: %g sec\n", duration);

    free(A);
    free(Anew);
}
