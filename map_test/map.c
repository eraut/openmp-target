#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

int main(int argc, char *argv[])
{
    int n;
    if (argc >= 2)
        n = atoi(argv[1]);
    else
        n = 10;
    double *A = malloc(sizeof(double)*n*n);
    //double A[10*10];

    // Fill with random data
    srand(1);
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            A[i*n+j] = ((double)rand())/RAND_MAX;
        }
    }

    double A0 = -1.0;
#pragma omp target map(to: A[0:n*n]) map(from: A0)
    {
        A0 = A[0];
    }
    printf("%g %g\n", A[0], A0);

    free(A);
}
