#include <stdio.h>
#include <stdint.h>

int main()
{
    size_t n = 10000000000;
    uint64_t sum = 0;
    #pragma omp target map(sum) nowait
    for (size_t i = 0; i < n; i++) {
        sum += (i/2);
    }
    printf("Waiting\n");
    #pragma omp taskwait
    printf("Done %lu\n", sum);
    return 0;
}
