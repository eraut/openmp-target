#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define IDX(i,j,k) ((i)*n2+(j)*n+(k))

int main(int argc, char *argv[])
{
    int n;
    if (argc >= 2)
        n = atoi(argv[1]);
    else
        n = 100;

    const int n2 = n*n;
    const int n3 = n2*n;

    double *A    = malloc(n3*sizeof(double)),
           *Anew = malloc(n3*sizeof(double));

    // Fill A (inners) with data
    #ifdef OMP_FT_OPT
    printf("Doing NUMA first-touch optimization\n");
    #pragma omp parallel for collapse(3) schedule(static)
    #endif
    for (int i = 1; i < n-1; i++) {
        for (int j = 1; j < n-1; j++) {
            for (int k = 1; k < n-1; k++) {
                A[IDX(i,j,k)] = 2.0*IDX(i,j,k);
                Anew[IDX(i,j,k)] = 0.0;
            }
        }
    }

    printf("Initializing boundary\n");

    // Fill A (boundary) with data
    #pragma omp parallel for collapse(3)
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            for (int k = 0; k < n; k++) {
                if (i == 0 || i == n-1 || j == 0 || j == n-1 || k == 0 ||
                    k == n-1) {
                    A[IDX(i,j,k)] = 0.0;
                    Anew[IDX(i,j,k)] = 0.0;
                }
            }
        }
    }

    const double tol = -1.0;
    double error = 1000.0;

    printf("Completed initialization\n");

    // Timekeeping
    struct timespec start,end;
    clock_gettime(CLOCK_MONOTONIC, &start);

    const int iter_max = 5;
    int iter = 0;
    while ( error > tol && iter < iter_max ) {
        error = 0.0;
        #pragma omp parallel for collapse(3) schedule(static) reduction(max:error)
        for (int i = 1; i < n-1; i++) {
            for (int j = 1; j < n-1; j++) {
                for (int k = 1; k < n-1; k++) {
                    Anew[IDX(i,j,k)] = (1./6.) *
                        (A[IDX(i+1,j,k)] + A[IDX(i-1,j,k)] +
                         A[IDX(i,j+1,k)] + A[IDX(i,j-1,k)] +
                         A[IDX(i,j,k+1)] + A[IDX(i,j,k-1)]);
                    error = fmax(error, fabs(Anew[IDX(i,j,k)] - A[IDX(i,j,k)]));
                }
            }
        }

        // Update A
        #pragma omp parallel for collapse(3) schedule(static)
        for (int i = 1; i < n-1; i++) {
            for (int j = 1; j < n-1; j++) {
                for (int k = 1; k < n-1; k++) {
                    A[IDX(i,j,k)] = Anew[IDX(i,j,k)];
                }
            }
        }

        ++iter;
        //if (iter % 100 == 0)
            printf("Iteration %d, error = %g\n", iter, error);
    }

    clock_gettime(CLOCK_MONOTONIC, &end);
    printf("Final error: %g\n", error);
    double duration = (end.tv_sec  - start.tv_sec) +
                    (double)(end.tv_nsec - start.tv_nsec) / 1.0e9;
    printf("Time elapsed: %g sec\n", duration);

    free(A);
    free(Anew);
}
